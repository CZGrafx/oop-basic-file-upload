<?php

echo "FILE: " . __FILE__ . "<br />";
echo "LINE: " . __LINE__ . "<br />";
echo "DIRECTORY: " . __DIR__ . "<br />";
echo "FUNCTION: " . __FUNCTION__ . "<br />";
echo "CLASS: " . __CLASS__ . "<br />";
echo "TRAIT: " . __TRAIT__ . "<br />";
echo "METHOD: " . __METHOD__ . "<br />";
echo "NAMESPACE: " . __NAMESPACE__ . "<br />";

if(file_exists(__DIR__)) {
    echo "Exists - " . __DIR__ . "<br />";
}

if(is_file(__DIR__)) {
    echo "YES, File.<br />";
} else {
    echo "No, File.<br />";
}

if(is_file(__FILE__)) {
    echo "YES, File.<br />";
} else {
    echo "No, File.<br />";
}

// TERTIARY
echo file_exists(__FILE__) ? "yes" : "no";
?>